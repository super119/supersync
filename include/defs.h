#ifndef _DEFS_H_
#define _DEFS_H_

#include <Windows.h>
#include <tchar.h>
#include <map>
using namespace std;

// =============================================================  Macros
#define ss_goto_if_fail(expr, label)                \
    if (!(expr)) goto label;

#define ss_return_if_fail(expr)                        \
    if (!(expr)) return;

#define ss_return_val_if_fail(expr, value)            \
    if (!(expr)) return value;

#define SSLOG(level, format, ...)                    \
    ss_log(level, TEXT(__FILE__), __LINE__, format, __VA_ARGS__);

#define SSFREE(p)                       \
    if ((p) != NULL) {                  \
        free(p);                        \
        (p) = NULL;                     \
    }

typedef unsigned (__stdcall *PTHREAD_START) (void *);
#define SSBEGINTHREADEX(psa, cbStackSize, pfnStartAddr, pvParam, fdwCreateFlags, pdwThreadID)    \
      ((HANDLE) _beginthreadex(                     \
         (void *) (psa),                            \
         (unsigned) (cbStackSize),                  \
         (PTHREAD_START) (pfnStartAddr),            \
         (void *) (pvParam),                        \
         (unsigned) (fdwCreateFlags),                \
         (unsigned *) (pdwThreadID)))

// =============================================================  Definitions
#define SS_REG_VALUE_MAX                        256
#define SUPER_SYNC_PROGRAM_GUID                 TEXT("{BD7F3111-5C48-40a9-AAF4-F1528376E909}")
#define SS_REGISTRY_KEY                         TEXT("Software\\SuperSync")
#define SS_REGKEY_ROOTDIR                       TEXT("rootdir")
#define SS_LOG_FILENAME                         TEXT("logfile")
#define SS_REG_LOGLEVEL_THRESHOLD               TEXT("loglevel")
#define SS_REG_SYNC_TO                          TEXT("syncto")
#define SS_SYNC_ID_FILENAME                     TEXT(".supersync.id")
#define SS_TIMER_HIDE_INIT_DIALOG               1
#define SS_NOTIFY_ICON_ID                       1

// Custom messages
#define WM_SS_NOTIFY_ICON_MSG                   WM_APP + 10
#define WM_SS_SYNC_COMPLETE                     WM_APP + 11

typedef enum _SSErrorCode
{
    SSE_NO_ERROR = 0, 
    SSE_SINGLETON_CHECK_FAILED,
    SSE_FIND_ROOTDIR_FAILED,
    SSE_INIT_LOGGER_FAILED, 
    SSE_INVALID_SYNC_TO, 
    SSE_CREATE_SYNC_THREAD_FAILED, 
    SSE_READ_FOLDER_FINFO_FAILED, 
    SSE_NEVER_BE_USED
} SSErrorCode;

typedef struct _SSConfigItems
{
    TCHAR *sync_to;   // Destination directory which sync to
} SSConfigItems;

typedef struct _SSFileInfo {
    TCHAR file_name[MAX_PATH];
    FILETIME last_write_time;
    DWORD file_size_high;
    DWORD file_size_low;
    BOOL is_folder;  // whether this file is an empty folder
} SSFileInfo;

// Compare function for map's PTSTR key
struct SSMapPtstrKeyCmp
{
    bool operator()(const PTSTR s1, const PTSTR s2) const
    {
        return _tcscmp(s1, s2) < 0;
    }
};

typedef struct _SSThreadArgs
{
    BOOL terminate;
    BOOL active;  // whether start working?
    CRITICAL_SECTION cs;
    HWND dialog_hwnd;

    SSConfigItems *sci;
    TCHAR drive_letter;  // The u-disk drive letter
    map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp> *sync_to_finfos;
} SSThreadArgs;


#endif