#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <defs.h>
#include <Windows.h>
#include <tchar.h>

typedef enum _LOG_LEVEL
{
    LOG_LEVEL_INFO,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_ERROR
} LOG_LEVEL;

/*
 * Set log file root directory and filename
 * Logger will create a "log" directory under root dir, then create log file
 * under it, using the filename which specified.
 * CAUTION: call this function first to make logger available
 *          next call this function will be ignore
 * NOT MT safe, call this function before create threads
 */
BOOL ss_logger_enable(PCTSTR root_dir, PCTSTR filename);

/*
 * Log string.
 * MT Safe
 */
void ss_log(LOG_LEVEL log_level, LPCTSTR filename, int line, LPCTSTR format, ...);

/*
 * Check whether the logger is enabled
 * MT Safe
 */
BOOL ss_logger_is_enabled();

/**
 * Set log level threshold
 */
void ss_logger_set_level_threshold(LOG_LEVEL threshold);

#endif