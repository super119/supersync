#ifndef _TH_SYNC_H_
#define _TH_SYNC_H_

#include <Windows.h>

// Thread main function
DWORD WINAPI sync_thread(PVOID pvParam);

#endif