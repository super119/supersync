#ifndef _UTILS_H_
#define _UTILS_H_

#include <defs.h>
#include <Windows.h>
#include <tchar.h>

/**
 * Find out root dir of the program via Registry.
 */
BOOL ss_utils_find_root_dir(PTSTR root_dir);

/**
 * Check whether the string is empty
 */
BOOL ss_utils_is_string_empty(PCTSTR input);

/*
 * Trim the leading spaces in the string
 */
TCHAR *ss_utils_trim_left(TCHAR *input);

/*
 * Trim the trailing spaces in the string
 */
TCHAR *ss_utils_trim_right(TCHAR *input);

/*
 * Trim the left & right spaces of string
 */
TCHAR *ss_utils_trim_both(TCHAR *input);

/**
 * Check whether the input file exists
 */
BOOL ss_utils_file_exists(PCTSTR filepath);

/**
 * Check whether the input directory exists
 */
BOOL ss_utils_dir_exists(PCTSTR dirpath);

/**
 * Check whether the directory is empty(no files in it).
 * Return TRUE means operation success
 * @empty param saves the result -- directory empty or not
 */
BOOL ss_utils_is_dir_empty(PCTSTR dirpath, BOOL *empty);

/**
 * Convert an error number to string
 * Not thread safe because a global error string buffer is used
 * And subsequence call to this function overwrites the previous error buffer
 */
PTSTR ss_utils_format_error_string(const DWORD code);

/**
 * Check out value from Registry
 */
BOOL ss_utils_get_registry_value(PCTSTR key, PCTSTR entry, PTSTR value, DWORD *valsize);

/**
 * Convert TCHAR string to ANSI string
 * CAUTION: free the result "to" after used
 */
BOOL ss_utils_convert_tchar_to_char(PCTSTR from, char **to);

/**
 * Read the folder contents and insert into map
 */
BOOL ss_utils_read_folder(map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp> &container, PCTSTR folder);

/**
 * Get the upper level directory path
 */
BOOL ss_utils_get_upper_dir(PCTSTR dir, PTSTR result, BOOL *no_upper_dir);

/**
 * Get the directory path of the file
 */
BOOL ss_utils_get_file_dir(PCTSTR filepath, PTSTR result);

/**
 * Create the directory recursively. (like "mkdir -p" in Linux)
 */
BOOL ss_utils_create_directory(PCTSTR dirpath);

#endif