#include <Windows.h>
#include <strsafe.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
{
    HANDLE hFind;
    WIN32_FIND_DATA wfd;
    TCHAR buf[4096];

    hFind = FindFirstFile(TEXT("C:\\*.*"), &wfd);
    if (hFind == INVALID_HANDLE_VALUE) {
        MessageBox(NULL, TEXT("FindFirstFile failed"), TEXT("Failed"), MB_OK | MB_ICONSTOP);
        return 1;
    } else {
        StringCchPrintf(buf, _countof(buf), TEXT("First file name: %s, size: %lld"), 
            wfd.cFileName, (wfd.nFileSizeHigh * (MAXDWORD + 1)) + wfd.nFileSizeLow);
        MessageBox(NULL, buf, TEXT("Info"), MB_OK | MB_ICONINFORMATION);

        while (FindNextFile(hFind, &wfd)) {
            StringCchPrintf(buf, _countof(buf), TEXT("First file name: %s, size: %lld"), 
                wfd.cFileName, (wfd.nFileSizeHigh * (MAXDWORD + 1)) + wfd.nFileSizeLow);
            MessageBox(NULL, buf, TEXT("Info"), MB_OK | MB_ICONINFORMATION);
        }
    }

    return 0;
}