//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SuperSync.rc
//
#define IDD_MAINDIALOG                  101
#define IDI_SSLOGO                      110
#define IDR_NIMENU                      114
#define IDC_STATIC_UGUID                1002
#define IDC_STATIC_UDISK_DRIVE_LETTER   1002
#define IDC_STATIC_SYNCTO               1003
#define IDC_STATIC_SYNC_ITEM            1004
#define IDC_PROGRESS_SYNC               1005
#define IDC_LOGOPNG                     1006
#define ID_SUPERSYNC_SHOWTHEDIALOG      40001
#define ID_SUPERSYNC_SYNCTHEU           40002
#define ID_SUPERSYNC_EXIT               40003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        115
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
