#include <Windows.h>
#include <Dbt.h>
#include <tchar.h>
#include <process.h>
#include <GdiPlus.h>
#include <defs.h>
#include <utils.h>
#include <logger.h>
#include <thsync.h>
#include <strsafe.h>
#include <commctrl.h>
#include <resource.h>
#include <map>
using namespace Gdiplus;
using namespace std;

HINSTANCE g_hInstance;
// Program configurations, in registry
SSConfigItems sci;
// Sync thread related
HANDLE th_sync = NULL;
DWORD th_sync_id;
SSThreadArgs targs;

static BOOL CALLBACK DlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
    switch(Message)
    {
    case WM_INITDIALOG:
        SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO)));
        SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO)));

        {
            // Place the dialog in the center of screen
            // Get the owner window and dialog box rectangles.
            HWND hwndOwner;
            RECT rcOwner, rcDlg, rc;
            if ((hwndOwner = GetParent(hwnd)) == NULL)
                hwndOwner = GetDesktopWindow();

            GetWindowRect(hwndOwner, &rcOwner);
            GetWindowRect(hwnd, &rcDlg);
            CopyRect(&rc, &rcOwner);

            // Offset the owner and dialog box rectangles so that right and bottom 
            // values represent the width and height, and then offset the owner again 
            // to discard space taken up by the dialog box. 
            OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
            OffsetRect(&rc, -rc.left, -rc.top);
            OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

            // The new position is the sum of half the remaining space and the owner's 
            // original position.
            SetWindowPos(hwnd, HWND_TOP, 
                rcOwner.left + (rc.right / 2), 
                rcOwner.top + (rc.bottom / 2), 
                0, 0,          // Ignores size arguments. 
                SWP_NOSIZE);

            SendMessage(GetDlgItem(hwnd, IDC_PROGRESS_SYNC), PBM_SETPOS, 0, 0);

            // Create a timer, hide the dialog to system tray when timer expires
            UINT_PTR timer = SetTimer(hwnd, SS_TIMER_HIDE_INIT_DIALOG, 1000, NULL);
            if (timer == 0) {
                SSLOG(LOG_LEVEL_WARNING, TEXT("Create hide init dialog timer failed."));
            }

            // Save the dialog HWND to thread args cause sync thread
            // need it to send message to dialog
            targs.dialog_hwnd = hwnd;
        }
        break;
    case WM_CLOSE:
        ShowWindow(hwnd, SW_HIDE);
        break;
    case WM_PAINT:
        // Draw the application logo(png) into the dialog
        {  // to avoid the "variable definition skipped by case" compiling error
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);
            RECT logo_rect, dlg_rect;
            GetWindowRect(GetDlgItem(hwnd, IDC_LOGOPNG), &logo_rect);
            GetWindowRect(hwnd, &dlg_rect);
            
            Graphics graphics(hdc);
            Image image(L"main.png", FALSE);
            graphics.DrawImage(&image, logo_rect.left - dlg_rect.left, logo_rect.top - dlg_rect.top);
            EndPaint(hwnd, &ps);
        }
        break;
    case WM_TIMER:
        if (wParam == SS_TIMER_HIDE_INIT_DIALOG) {
            // Hide the dialog to system tray
            NOTIFYICONDATA nidata;
            memset(&nidata, 0, sizeof(nidata));
            nidata.cbSize = sizeof(NOTIFYICONDATA);
            nidata.hWnd = hwnd;
            nidata.uID = SS_NOTIFY_ICON_ID;
            nidata.uFlags = NIF_ICON | NIF_MESSAGE | NIF_INFO;
            nidata.uCallbackMessage = WM_SS_NOTIFY_ICON_MSG;
            nidata.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO));
            StringCchCopy(nidata.szInfo, _countof(nidata.szInfo), TEXT("SuperSync准备OK，等待U盘事件中..."));
            nidata.uTimeout = 10000;
            StringCchCopy(nidata.szInfoTitle, _countof(nidata.szInfoTitle), TEXT("SuperSync Ready"));
            nidata.dwInfoFlags = NIIF_INFO;

            if (Shell_NotifyIcon(NIM_ADD, &nidata) == FALSE) {
                SSLOG(LOG_LEVEL_WARNING, TEXT("Add notify icon to system tray failed. Reason: %s"), ss_utils_format_error_string(GetLastError()));
            } else {
                ShowWindow(hwnd, SW_HIDE);
            }
            // Anyway, timer doesn't make sense any more
            KillTimer(hwnd, SS_TIMER_HIDE_INIT_DIALOG);
        }
        break;
    case WM_DEVICECHANGE:
        switch (wParam) {
        case DBT_DEVICEARRIVAL:
            {
                DEV_BROADCAST_HDR *lpDevHdr = (DEV_BROADCAST_HDR *)lParam;
                if(lpDevHdr->dbch_devicetype == DBT_DEVTYP_VOLUME) {
                    DEV_BROADCAST_VOLUME *dbv = (DEV_BROADCAST_VOLUME *)lParam;
                    // Check out the disk driver letter
                    // For simple, we just check out the first drive letter
                    int i = 0;
                    TCHAR drive_letter;
                    while (TRUE) {
                        if (dbv->dbcv_unitmask & (1 << i)) break;
                        i++;
                    }
                    drive_letter = TEXT('A' + i);
                    SSLOG(LOG_LEVEL_INFO, TEXT("Got the U disk driver letter(first partition): %c"), drive_letter);
                    // Check whether this is a supersync disk
                    TCHAR super_sync_id_file[MAX_PATH];
                    StringCchPrintf(super_sync_id_file, _countof(super_sync_id_file), TEXT("%c:\\%s"), drive_letter, SS_SYNC_ID_FILENAME);
                    if (ss_utils_file_exists(super_sync_id_file)) {
                        SSLOG(LOG_LEVEL_INFO, TEXT("SuperSync disk found, start syncing..."));

                        NOTIFYICONDATA nidata;
                        memset(&nidata, 0, sizeof(nidata));
                        nidata.cbSize = sizeof(NOTIFYICONDATA);
                        nidata.hWnd = hwnd;
                        nidata.uID = SS_NOTIFY_ICON_ID;
                        nidata.uFlags = NIF_ICON | NIF_MESSAGE | NIF_INFO;
                        nidata.uCallbackMessage = WM_SS_NOTIFY_ICON_MSG;
                        nidata.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO));
                        StringCchCopy(nidata.szInfo, _countof(nidata.szInfo), TEXT("找到SuperSync disk，开始同步......"));
                        nidata.uTimeout = 10000;
                        StringCchCopy(nidata.szInfoTitle, _countof(nidata.szInfoTitle), TEXT("开始同步"));
                        nidata.dwInfoFlags = NIIF_INFO;

                        if (Shell_NotifyIcon(NIM_MODIFY, &nidata) == FALSE) {
                            SSLOG(LOG_LEVEL_WARNING, TEXT("Modify notify icon to system tray failed. Reason: %s"), ss_utils_format_error_string(GetLastError()));
                        }

                        // Start working
                        EnterCriticalSection(&(targs.cs));
                        targs.drive_letter = drive_letter;
                        targs.active = TRUE;
                        LeaveCriticalSection(&(targs.cs));
                    } else {
                        SSLOG(LOG_LEVEL_INFO, TEXT("Not supersync disk, ignore this disk..."));
                    }
                }
            }
            break;
        default:
            break;
        }
        break;
    case WM_SS_NOTIFY_ICON_MSG:
        // wParam is the ID of the system tray icon, we just have 1 so ignore it
        // lParam is the event
        switch (lParam) {
        case WM_LBUTTONDBLCLK:
        case WM_LBUTTONDOWN:
            ShowWindow(hwnd, SW_SHOW);
            break;
        case WM_RBUTTONUP:
            {
                POINT pos;
                GetCursorPos(&pos);
                HMENU hMenu = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDR_NIMENU));
                TrackPopupMenu(GetSubMenu(hMenu, 0), 0, pos.x, pos.y, 0, hwnd, NULL);
            }
            break;
        default:
            break;
        }
        break;
    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case ID_SUPERSYNC_SHOWTHEDIALOG:
            ShowWindow(hwnd, SW_SHOW);
            break;
        case ID_SUPERSYNC_SYNCTHEU:
            // Start working
            // Check whether the sync is ongoing, if so, ignore this
            if (targs.active) {
                SSLOG(LOG_LEVEL_WARNING, TEXT("Syncing is ongoing, can't sync the disk right now..."));
                NOTIFYICONDATA nidata;
                memset(&nidata, 0, sizeof(nidata));
                nidata.cbSize = sizeof(NOTIFYICONDATA);
                nidata.hWnd = hwnd;
                nidata.uID = SS_NOTIFY_ICON_ID;
                nidata.uFlags = NIF_ICON | NIF_MESSAGE | NIF_INFO;
                nidata.uCallbackMessage = WM_SS_NOTIFY_ICON_MSG;
                nidata.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO));
                StringCchCopy(nidata.szInfo, _countof(nidata.szInfo), TEXT("正在同步中，忽略本次同步请求......"));
                nidata.uTimeout = 10000;
                StringCchCopy(nidata.szInfoTitle, _countof(nidata.szInfoTitle), TEXT("同步中"));
                nidata.dwInfoFlags = NIIF_INFO;

                Shell_NotifyIcon(NIM_MODIFY, &nidata);
            } else {
                EnterCriticalSection(&(targs.cs));
                targs.active = TRUE;
                LeaveCriticalSection(&(targs.cs));
            }
            break;
        case ID_SUPERSYNC_EXIT:
            {
                // Check whether the sync is ongoing, if so, ignore this
                if (targs.active) {
                    SSLOG(LOG_LEVEL_WARNING, TEXT("Syncing is ongoing, can't quit the program right now..."));
                    NOTIFYICONDATA nidata;
                    memset(&nidata, 0, sizeof(nidata));
                    nidata.cbSize = sizeof(NOTIFYICONDATA);
                    nidata.hWnd = hwnd;
                    nidata.uID = SS_NOTIFY_ICON_ID;
                    nidata.uFlags = NIF_ICON | NIF_MESSAGE | NIF_INFO;
                    nidata.uCallbackMessage = WM_SS_NOTIFY_ICON_MSG;
                    nidata.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO));
                    StringCchCopy(nidata.szInfo, _countof(nidata.szInfo), TEXT("正在同步中，现在无法退出程序......"));
                    nidata.uTimeout = 10000;
                    StringCchCopy(nidata.szInfoTitle, _countof(nidata.szInfoTitle), TEXT("同步中"));
                    nidata.dwInfoFlags = NIIF_INFO;

                    Shell_NotifyIcon(NIM_MODIFY, &nidata);
                } else {
                    NOTIFYICONDATA nidata;
                    memset(&nidata, 0, sizeof(nidata));
                    nidata.cbSize = sizeof(NOTIFYICONDATA);
                    nidata.hWnd = hwnd;
                    nidata.uID = SS_NOTIFY_ICON_ID;
                    if (Shell_NotifyIcon(NIM_DELETE, &nidata) == FALSE) {
                        SSLOG(LOG_LEVEL_WARNING, TEXT("Remove notify icon failed. Reason: %s"), ss_utils_format_error_string(GetLastError()));
                    }

                    EndDialog(hwnd, SSE_NO_ERROR);
                }
            }
            break;
        default:
            SSLOG(LOG_LEVEL_WARNING, TEXT("Illegal Notify Icon Menu Item: %d"), LOWORD(wParam));
            break;
        }
        break;
    case WM_SS_SYNC_COMPLETE:
        {
            NOTIFYICONDATA nidata;
            memset(&nidata, 0, sizeof(nidata));
            nidata.cbSize = sizeof(NOTIFYICONDATA);
            nidata.hWnd = hwnd;
            nidata.uID = SS_NOTIFY_ICON_ID;
            nidata.uFlags = NIF_ICON | NIF_MESSAGE | NIF_INFO;
            nidata.uCallbackMessage = WM_SS_NOTIFY_ICON_MSG;
            nidata.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SSLOGO));
            StringCchCopy(nidata.szInfo, _countof(nidata.szInfo), TEXT("SuperSync同步完成......"));
            nidata.uTimeout = 20000;
            StringCchCopy(nidata.szInfoTitle, _countof(nidata.szInfoTitle), TEXT("同步完成"));
            nidata.dwInfoFlags = NIIF_INFO;

            Shell_NotifyIcon(NIM_MODIFY, &nidata);
        }
        break;
    default:
        return FALSE;  // We didn't process this message
    }
    return TRUE;  // We processed the message
}

static SSErrorCode read_configs()
{
    TCHAR reg_value[SS_REG_VALUE_MAX];
    DWORD reg_value_size;

    memset(&sci, 0, sizeof(SSConfigItems));

    reg_value_size = SS_REG_VALUE_MAX * sizeof(TCHAR);
    if (ss_utils_get_registry_value(SS_REGISTRY_KEY, SS_REG_SYNC_TO, reg_value, &reg_value_size)) {
        SSLOG(LOG_LEVEL_INFO, TEXT("Got config: SYNC TO is: %s"), reg_value);
        if (ss_utils_is_string_empty(reg_value)) {
            SSLOG(LOG_LEVEL_ERROR, TEXT("SYNC TO not set, quit."));
            MessageBox(NULL, TEXT("同步目的目录没有设置，程序退出"), TEXT("错误"), MB_OK | MB_ICONSTOP);
            return SSE_INVALID_SYNC_TO;
        }
        // Set to sci
        sci.sync_to = (TCHAR *)malloc(reg_value_size);
        memcpy(sci.sync_to, reg_value, reg_value_size);
        // remove trailing slash
        if (sci.sync_to[_tcslen(sci.sync_to) - 1] == (TCHAR)'\\') {
            sci.sync_to[_tcslen(sci.sync_to) - 1] = (TCHAR)'\0';
        }
    } else {
        SSLOG(LOG_LEVEL_ERROR, TEXT("Get config: SYNC TO failed, quit."));
        MessageBox(NULL, TEXT("获取同步目的目录失败，程序退出"), TEXT("错误"), MB_OK | MB_ICONSTOP);
        return SSE_INVALID_SYNC_TO;
    }

    return SSE_NO_ERROR;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
{
    TCHAR root_dir[MAX_PATH];
    SSErrorCode ret = SSE_NO_ERROR;
    TCHAR reg_value[SS_REG_VALUE_MAX];
    DWORD reg_value_size;
    GdiplusStartupInput gdiplus_startup_input;
    ULONG_PTR gdiplus_token;
    map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp> sync_to_finfos;
    map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp>::iterator mapit;

    // Make sure only 1 instance of the program is running
    HANDLE singleton_mutex = CreateMutex(NULL, FALSE, SUPER_SYNC_PROGRAM_GUID);
    if (GetLastError() == ERROR_ALREADY_EXISTS) {
        MessageBox(NULL, TEXT("程序已经运行，请不要重复启动"), TEXT("错误"), MB_OK | MB_ICONSTOP);
        return SSE_SINGLETON_CHECK_FAILED;
    }

    g_hInstance = hInstance;

    // Find out the root directory of the program
    // Root directory is checked out from Registry
    // Install program should create the root dir in Registry
    if (!ss_utils_find_root_dir(root_dir)) {
        MessageBox(NULL, TEXT("没有找到程序的安装目录，请检查注册表设置"), TEXT("错误"), MB_OK | MB_ICONSTOP);
        if (singleton_mutex != NULL) CloseHandle(singleton_mutex);
        return SSE_FIND_ROOTDIR_FAILED;
    }

    // Init logger
    if (!ss_logger_enable(root_dir, SS_LOG_FILENAME)) {
        MessageBox(NULL, TEXT("初始化logger失败，程序退出"), TEXT("错误"), MB_OK | MB_ICONSTOP);
        if (singleton_mutex != NULL) CloseHandle(singleton_mutex);
        return SSE_INIT_LOGGER_FAILED;
    }
    // Check out log level threshold
    reg_value_size = SS_REG_VALUE_MAX * sizeof(TCHAR);
    if (!ss_utils_get_registry_value(SS_REGISTRY_KEY, SS_REG_LOGLEVEL_THRESHOLD, reg_value, &reg_value_size)) {
        SSLOG(LOG_LEVEL_WARNING, TEXT("Get log level threshold from Registry failed. Use default log level INFO."));
        ss_logger_set_level_threshold(LOG_LEVEL_INFO);
    } else {
        if (_tcsncmp(reg_value, TEXT("INFO"), 4) == 0) {
            ss_logger_set_level_threshold(LOG_LEVEL_INFO);
        } else if (_tcsncmp(reg_value, TEXT("WARN"), 4) == 0) {
            ss_logger_set_level_threshold(LOG_LEVEL_WARNING);
        } else if (_tcsncmp(reg_value, TEXT("ERRO"), 4) == 0) {
            ss_logger_set_level_threshold(LOG_LEVEL_ERROR);
        } else {
            ss_logger_set_level_threshold(LOG_LEVEL_INFO);
        }
    }

    // Init GDIPLUS
    GdiplusStartup(&gdiplus_token, &gdiplus_startup_input, NULL);

    SSLOG(LOG_LEVEL_INFO, TEXT("Program started. Read program configs next."));
    // Read configs
    ret = read_configs();
    if (ret != SSE_NO_ERROR) goto TERMINATE;

    SSLOG(LOG_LEVEL_INFO, TEXT("Read config finished. Read sync to folder contents..."));
    // Read sync to folder contents, construct sync to file info structure list
    if (ss_utils_read_folder(sync_to_finfos, sci.sync_to) == FALSE) {
        SSLOG(LOG_LEVEL_ERROR, TEXT("Read sync to folder content failed. Program quit."));
        ret = SSE_READ_FOLDER_FINFO_FAILED;
        goto TERMINATE;
    }
    /* {
        // Print out all folder stuffs, just for debugging
        mapit = sync_to_finfos.begin();
        while (mapit != sync_to_finfos.end()) {
            TCHAR buf[1024];
            StringCchPrintf(buf, _countof(buf), TEXT("File: %s, size: %d|%d, last write time: %d|%d"), 
                (PTSTR)(mapit->first), ((SSFileInfo *)(mapit->second))->file_size_high, 
                ((SSFileInfo *)(mapit->second))->file_size_low, ((SSFileInfo *)(mapit->second))->last_write_time.dwHighDateTime, 
                ((SSFileInfo *)(mapit->second))->last_write_time.dwLowDateTime);
            SSLOG(LOG_LEVEL_INFO, TEXT("%s"), buf);

            mapit++;
        }
    } */
    
    // Create the sync thread, inactive default.
    SSLOG(LOG_LEVEL_INFO, TEXT("Read sync to folder contents finished. Create sync thread next."));
    memset(&targs, 0, sizeof(SSThreadArgs));
    targs.sci = &sci;
    targs.terminate = FALSE;
    targs.active = FALSE;  // default sleeping
    InitializeCriticalSection(&(targs.cs));
    targs.sync_to_finfos = &sync_to_finfos;

    th_sync_id = 0;
    th_sync = SSBEGINTHREADEX(NULL, 0, sync_thread, &targs, 0, &th_sync_id);
    if (th_sync <= 0) {
        SSLOG(LOG_LEVEL_ERROR, TEXT("Create sync thread failed. Reason: %s"), ss_utils_format_error_string(GetLastError()));
        MessageBox(NULL, TEXT("创建SYNC线程失败，请检查日志查看详细信息"), TEXT("错误"), MB_OK | MB_ICONSTOP);
        ret = SSE_CREATE_SYNC_THREAD_FAILED;
        goto TERMINATE;
    }

    // Create the interface
    SSLOG(LOG_LEVEL_INFO, TEXT("Sync thread create finished. Construct the dialog and show it."));
    INT_PTR dlg_ret = DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAINDIALOG), NULL, DlgProc);
    ret = (SSErrorCode)dlg_ret;

TERMINATE:
    SSLOG(LOG_LEVEL_INFO, TEXT("Program terminating. Close the thread and release the resources."));
    // Terminate the threads
    if (th_sync > 0) {
        targs.terminate = TRUE;
        WaitForSingleObject(th_sync, INFINITE);
        CloseHandle(th_sync);

        DeleteCriticalSection(&(targs.cs));
    }

    // Free the map
    mapit = sync_to_finfos.begin();
    while (mapit != sync_to_finfos.end()) {
        free(mapit->first);
        free(mapit->second);

        mapit++;
    }

    // Free others
    SSFREE(sci.sync_to);
    GdiplusShutdown(gdiplus_token);

    if (singleton_mutex != NULL) CloseHandle(singleton_mutex);

    SSLOG(LOG_LEVEL_INFO, TEXT("Program halted. The final return result is %d"), ret);
    return ret;
}