#include <thsync.h>
#include <logger.h>
#include <utils.h>
#include <defs.h>
#include <strsafe.h>
#include <commctrl.h>
#include <resource.h>

DWORD WINAPI sync_thread(PVOID pvParam)
{
    SSThreadArgs *targs = (SSThreadArgs *)pvParam;
    map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp> udisk_finfos;
    map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp>::iterator ufmapit;
    map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp>::iterator stmapit;
    TCHAR udisk_path[MAX_PATH];
    TCHAR filepath_with_syncto_syntax[MAX_PATH];
    TCHAR filepath_with_udisk_syntax[MAX_PATH];
    TCHAR processing_text[MAX_PATH + 100];  // for display in interface, indicates which file is processing
    TCHAR dir_need_create[MAX_PATH];
    int file_synced = 0;
    float file_synced_percent = 0;
    int file_need_sync = 0;

    while (!(targs->terminate)) {
        if (targs->active) {
            // Read the u-disk contents first
            StringCchPrintf(udisk_path, _countof(udisk_path), TEXT("%c:\\"), targs->drive_letter);
            if (!ss_utils_read_folder(udisk_finfos, udisk_path)) {
                SSLOG(LOG_LEVEL_ERROR, TEXT("Read u-disk contents failed. Ignore this time syncing..."));
                goto SYNCFINISHED;
            }

            file_synced = 0;
            file_synced_percent = 0;
            file_need_sync = udisk_finfos.size();
            // Update the interface
            SetWindowText(GetDlgItem(targs->dialog_hwnd, IDC_STATIC_UDISK_DRIVE_LETTER), udisk_path);
            SetWindowText(GetDlgItem(targs->dialog_hwnd, IDC_STATIC_SYNCTO), targs->sci->sync_to);
            SendMessage(GetDlgItem(targs->dialog_hwnd, IDC_PROGRESS_SYNC), PBM_SETPOS, 0, 0);

            // Start syncing
            ufmapit = udisk_finfos.begin();
            while (ufmapit != udisk_finfos.end()) {
                // Whether this file exists in sync to folder?
                // Cause we save absolute file path in the map, so here 
                // we should replace the u-disk's drive letter to sync to folder
                StringCchPrintf(filepath_with_syncto_syntax, _countof(filepath_with_syncto_syntax), TEXT("%s\\%s"), 
                    targs->sci->sync_to, (PTSTR)(ufmapit->first) + _tcslen(udisk_path));
                StringCchPrintf(processing_text, _countof(processing_text), TEXT("[U�� >> Ӳ��]: %s"), filepath_with_syncto_syntax);
                SetWindowText(GetDlgItem(targs->dialog_hwnd, IDC_STATIC_SYNC_ITEM), processing_text);
                SSFileInfo *ussfi = (SSFileInfo *)ufmapit->second;

                if ((*(targs->sync_to_finfos)).count(filepath_with_syncto_syntax) == 0) {
                    // Whether this item is a folder?
                    if (ussfi->is_folder) {
                        // Create the folder is OK
                        ss_utils_create_directory(filepath_with_syncto_syntax);
                        // Add this empty folder to syncto's map
                        SSFileInfo *sssfi = (SSFileInfo *)malloc(sizeof(SSFileInfo));
                        StringCchCopy(sssfi->file_name, _countof(sssfi->file_name), filepath_with_syncto_syntax);
                        sssfi->file_size_high = ussfi->file_size_high;
                        sssfi->file_size_low = ussfi->file_size_low;
                        sssfi->last_write_time = ussfi->last_write_time;
                        sssfi->is_folder = ussfi->is_folder;
                        PTSTR key = (PTSTR)malloc((_tcslen(filepath_with_syncto_syntax) + 1) * sizeof(TCHAR));
                        memset(key, 0, (_tcslen(filepath_with_syncto_syntax) + 1) * sizeof(TCHAR));
                        StringCchCopy(key, _tcslen(filepath_with_syncto_syntax) + 1, filepath_with_syncto_syntax);
                        (*(targs->sync_to_finfos))[key] = sssfi;

                        goto USYNCNEXT;
                    }

                    // Make the directory anyway to avoid copy failed
                    if (ss_utils_get_file_dir(filepath_with_syncto_syntax, dir_need_create) == FALSE) {
                        SSLOG(LOG_LEVEL_WARNING, TEXT("Get dir of file: %s failed. Ignore syncing this file."), filepath_with_syncto_syntax);
                        goto USYNCNEXT;
                    }
                    if (ss_utils_create_directory(dir_need_create) == FALSE) {
                        SSLOG(LOG_LEVEL_WARNING, TEXT("Create directory in syncto folder failed. Ignore syncing this file: %s"), filepath_with_syncto_syntax);
                        goto USYNCNEXT;
                    }

                    // Copy the file to syncto folder
                    if (CopyFile(ufmapit->first, filepath_with_syncto_syntax, FALSE) == 0) {
                        SSLOG(LOG_LEVEL_ERROR, TEXT("Copy file %s to %s failed. Reason: %s. Ignore this time syncing."), 
                            ufmapit->first, filepath_with_syncto_syntax, ss_utils_format_error_string(GetLastError()));
                        break;
                    }

                    // Add this file to syncto's map
                    SSFileInfo *sssfi = (SSFileInfo *)malloc(sizeof(SSFileInfo));
                    StringCchCopy(sssfi->file_name, _countof(sssfi->file_name), filepath_with_syncto_syntax);
                    sssfi->file_size_high = ussfi->file_size_high;
                    sssfi->file_size_low = ussfi->file_size_low;
                    sssfi->last_write_time = ussfi->last_write_time;
                    sssfi->is_folder = ussfi->is_folder;
                    PTSTR key = (PTSTR)malloc((_tcslen(filepath_with_syncto_syntax) + 1) * sizeof(TCHAR));
                    memset(key, 0, (_tcslen(filepath_with_syncto_syntax) + 1) * sizeof(TCHAR));
                    StringCchCopy(key, _tcslen(filepath_with_syncto_syntax) + 1, filepath_with_syncto_syntax);
                    (*(targs->sync_to_finfos))[key] = sssfi;
                } else {
                    SSFileInfo *sssfi = (SSFileInfo *)((*(targs->sync_to_finfos))[filepath_with_syncto_syntax]);

                    // Whether this item is a folder?
                    if (ussfi->is_folder) {
                        // Empty folder is not needed to sync
                        sssfi->file_size_high = ussfi->file_size_high;
                        sssfi->file_size_low = ussfi->file_size_low;
                        sssfi->last_write_time = ussfi->last_write_time;
                        goto USYNCNEXT;
                    }

                    // Check the file last write time & file size
                    if ((ussfi->last_write_time.dwHighDateTime != sssfi->last_write_time.dwHighDateTime) || 
                        (ussfi->last_write_time.dwLowDateTime != sssfi->last_write_time.dwLowDateTime) || 
                        (ussfi->file_size_high != sssfi->file_size_high) || (ussfi->file_size_low != sssfi->file_size_low)) {
                        // Copy the file from u-disk and overwrite the file in syncto folder
                        if (CopyFile(ufmapit->first, filepath_with_syncto_syntax, FALSE) == 0) {
                            SSLOG(LOG_LEVEL_ERROR, TEXT("Copy file %s to %s failed. Reason: %s. Ignore this time syncing."), 
                                ufmapit->first, filepath_with_syncto_syntax, ss_utils_format_error_string(GetLastError()));
                            break;
                        }

                        // Change this file's info in syncto map
                        sssfi->file_size_high = ussfi->file_size_high;
                        sssfi->file_size_low = ussfi->file_size_low;
                        sssfi->last_write_time = ussfi->last_write_time;
                    }
                }

USYNCNEXT:
                ufmapit++;
                file_synced++;
                file_synced_percent = ((float)file_synced / (float)file_need_sync) * 100;
                SendMessage(GetDlgItem(targs->dialog_hwnd, IDC_PROGRESS_SYNC), PBM_SETPOS, (int)file_synced_percent, 0);
            }

            // Sync reverse, iterate from syncto map, remove the file/dir exists in syncto map while not in udisk's map
            SendMessage(GetDlgItem(targs->dialog_hwnd, IDC_PROGRESS_SYNC), PBM_SETPOS, 0, 0);
            file_synced = 0;
            file_synced_percent = 0;
            file_need_sync = targs->sync_to_finfos->size();

            stmapit = targs->sync_to_finfos->begin();
            while (stmapit != targs->sync_to_finfos->end()) {
                // Whether this file exists in u disk?
                // Cause we save absolute file path in the map, so here 
                // we should replace the syncto's path to udisk path
                StringCchPrintf(filepath_with_udisk_syntax, _countof(filepath_with_udisk_syntax), TEXT("%s%s"), 
                    udisk_path, (PTSTR)(stmapit->first) + _tcslen(targs->sci->sync_to) + 1);  // add 1 is the backslash after the targs->sci->sync_to
                StringCchPrintf(processing_text, _countof(processing_text), TEXT("[U�� << Ӳ��]: %s"), filepath_with_udisk_syntax);
                SetWindowText(GetDlgItem(targs->dialog_hwnd, IDC_STATIC_SYNC_ITEM), processing_text);
                SSFileInfo *sssfi = (SSFileInfo *)stmapit->second;

                if (udisk_finfos.count(filepath_with_udisk_syntax) == 0) {
                    // File doesn't exist in U disk, so remove it in syncto folder
                    if (sssfi->is_folder) {
                        // remove this empty folder
                        if (RemoveDirectory(sssfi->file_name) == 0) {
                            SSLOG(LOG_LEVEL_WARNING, TEXT("Remove empty folder: %s in syncto folder failed. Reason: %s"), sssfi->file_name, ss_utils_format_error_string(GetLastError()));
                            goto SSYNCNEXT;
                        }
                    } else {
                        // remove this file
                        if (DeleteFile(sssfi->file_name) == 0) {
                            SSLOG(LOG_LEVEL_WARNING, TEXT("Remove file: %s in syncto folder failed. Reason: %s"), sssfi->file_name, ss_utils_format_error_string(GetLastError()));
                            goto SSYNCNEXT;
                        }
                    }

                    // remove this entry in syncto map
                    stmapit = targs->sync_to_finfos->erase(stmapit);
                    file_synced++;
                    file_synced_percent = ((float)file_synced / (float)file_need_sync) * 100;
                    SendMessage(GetDlgItem(targs->dialog_hwnd, IDC_PROGRESS_SYNC), PBM_SETPOS, (int)file_synced_percent, 0);
                    continue;
                }

SSYNCNEXT:
                stmapit++;
                file_synced++;
                file_synced_percent = ((float)file_synced / (float)file_need_sync) * 100;
                SendMessage(GetDlgItem(targs->dialog_hwnd, IDC_PROGRESS_SYNC), PBM_SETPOS, (int)file_synced_percent, 0);
            }

SYNCFINISHED:
            // Sync finished, free the map
            ufmapit = udisk_finfos.begin();
            while (ufmapit != udisk_finfos.end()) {
                free(ufmapit->first);
                free(ufmapit->second);

                ufmapit++;
            }
            udisk_finfos.clear();
            // Sync finished, reset to inactive
            EnterCriticalSection(&(targs->cs));
            targs->active = FALSE;
            LeaveCriticalSection(&(targs->cs));
            // Send a message to main thread to make a system tray ballon to 
            // notify user this time syncing completed
            SendMessage(targs->dialog_hwnd, WM_SS_SYNC_COMPLETE, 0, 0);
        }
        Sleep(1000);
    }

    SSLOG(LOG_LEVEL_INFO, TEXT("Sync thread terminated..."));
    return SSE_NO_ERROR;
}