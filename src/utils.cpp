#include <utils.h>
#include <strsafe.h>
#include <logger.h>

// Save error string
#define ERROR_STRBUF_SIZE                4096
TCHAR error_string_buffer[ERROR_STRBUF_SIZE];

BOOL ss_utils_find_root_dir(PTSTR root_dir)
{
    HKEY reg_key;
    DWORD root_directory_len = MAX_PATH;
    LONG ret;

    // open the registry key
    ret = RegOpenKeyEx(HKEY_CURRENT_USER, SS_REGISTRY_KEY, 0, KEY_QUERY_VALUE, &reg_key);
    if (ret != ERROR_SUCCESS)
        goto regfailed;

    // query the key value
    ret = RegQueryValueEx(reg_key, SS_REGKEY_ROOTDIR, NULL, NULL, 
                          (LPBYTE)root_dir, &root_directory_len);
    if (ret != ERROR_SUCCESS)
        goto regfailed;

    // get root directory success
    RegCloseKey(reg_key);
    return TRUE;

regfailed:
    if (reg_key != NULL) RegCloseKey(reg_key);
    // Get current working directory as root directory
    if(!GetCurrentDirectory(MAX_PATH, root_dir))
        return FALSE;
    return TRUE;
}

BOOL ss_utils_is_string_empty(PCTSTR input)
{
    ss_return_val_if_fail(input != NULL, TRUE);

    while(*input) {
        if ((*input) == (TCHAR)' ')
            input++;
        else
            return FALSE;
    }

    return TRUE;
}

TCHAR *ss_utils_trim_left(TCHAR *input)
{
	ss_return_val_if_fail(input != NULL, NULL);

	while ((*input) == (TCHAR)' ')
		input++;

	return input;
}

TCHAR *ss_utils_trim_right(TCHAR *input)
{
	size_t input_string_len = 0;
	TCHAR *tmp = NULL;

	ss_return_val_if_fail(input != NULL, NULL);
	ss_return_val_if_fail(SUCCEEDED(StringCchLength(input, STRSAFE_MAX_CCH, &input_string_len)), NULL);
	tmp = input + input_string_len - 1;
	
	while ((*tmp) == (TCHAR)' ') {
		*tmp = (TCHAR)'\0';
		tmp--;
	}

	// we just return the original pointer because we only modifies
	// the trailing part of the string
	// we defined a return value for user check whether the function succeeded
	return input;
}

TCHAR *ss_utils_trim_both(TCHAR *input)
{
	TCHAR *tmp = NULL;
	ss_return_val_if_fail(input != NULL, NULL);

	tmp = ss_utils_trim_left(input);
	ss_return_val_if_fail(tmp != NULL, NULL);

	tmp = ss_utils_trim_right(tmp);
	ss_return_val_if_fail(tmp != NULL, NULL);

	return tmp;
}

BOOL ss_utils_file_exists(PCTSTR filepath)
{
    DWORD file_attr;

    ss_return_val_if_fail(filepath != NULL, FALSE);
    file_attr = GetFileAttributes(filepath);
    if (file_attr == INVALID_FILE_ATTRIBUTES) return FALSE;
    if (file_attr & FILE_ATTRIBUTE_DIRECTORY) return FALSE;

    return TRUE;
}

BOOL ss_utils_dir_exists(PCTSTR dirpath)
{
    DWORD file_attr;

    ss_return_val_if_fail(dirpath != NULL, FALSE);
    file_attr = GetFileAttributes(dirpath);
    if (file_attr == INVALID_FILE_ATTRIBUTES) return FALSE;

    if (file_attr & FILE_ATTRIBUTE_DIRECTORY) {
        return TRUE;
    } else { 
        return TRUE;
    }
}

BOOL ss_utils_is_dir_empty(PCTSTR dirpath, BOOL *empty)
{
    size_t folder_len;
    TCHAR folder_find_path[MAX_PATH];
    HANDLE hFind;
    WIN32_FIND_DATA wfd;
    
    ss_return_val_if_fail(empty != NULL, FALSE);
    if (dirpath == NULL) return FALSE;

    StringCchLength(dirpath, MAX_PATH, &folder_len);
    if (dirpath[folder_len - 1] == (TCHAR)'\\') {
        StringCchPrintf(folder_find_path, _countof(folder_find_path), TEXT("%s%s"), dirpath, TEXT("*.*"));
    } else {
        StringCchPrintf(folder_find_path, _countof(folder_find_path), TEXT("%s%s"), dirpath, TEXT("\\*.*"));
    }

    hFind = FindFirstFile(folder_find_path, &wfd);
    if (hFind == INVALID_HANDLE_VALUE) {
        SSLOG(LOG_LEVEL_ERROR, TEXT("FindFirstFile of folder: %s failed. Reason: %s"), folder_find_path, 
                                    ss_utils_format_error_string(GetLastError()));
        return FALSE;
    } else {
        // Ignore system file and dot files
        if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) || 
            (_tcscmp(wfd.cFileName, TEXT(".")) == 0) || 
            (_tcscmp(wfd.cFileName, TEXT("..")) == 0)) {
            goto FINDNEXT;
        }

        // Find one file here, directory is not empty
        *empty = FALSE;
        if (hFind && hFind != INVALID_HANDLE_VALUE) FindClose(hFind);
        return TRUE;
    }

FINDNEXT:
    while (FindNextFile(hFind, &wfd)) {
        if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) || 
            (_tcscmp(wfd.cFileName, TEXT(".")) == 0) || 
            (_tcscmp(wfd.cFileName, TEXT("..")) == 0)) {
            continue;
        }

        // Find one file here
        *empty = FALSE;
        if (hFind && hFind != INVALID_HANDLE_VALUE) FindClose(hFind);
        return TRUE;
    }

    if (hFind && hFind != INVALID_HANDLE_VALUE) FindClose(hFind);
    *empty = TRUE;
    return TRUE;
}

PTSTR ss_utils_format_error_string(const DWORD code)
{
    DWORD systemLocale;
    BOOL ret;
    size_t error_string_len;

    if (code < 0) {
        // illegal error code
        return NULL;
    }
    systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);

    // append the error code first
    ss_return_val_if_fail(SUCCEEDED(StringCchPrintf(error_string_buffer, ERROR_STRBUF_SIZE / sizeof(TCHAR), TEXT("(%d)"), code)), NULL);

    // append error string then
    ss_return_val_if_fail(SUCCEEDED(StringCchLength(error_string_buffer, ERROR_STRBUF_SIZE / sizeof(TCHAR), &error_string_len)), NULL);
    ret = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
                        NULL, code, systemLocale, (error_string_buffer + error_string_len),
                        ERROR_STRBUF_SIZE / sizeof(TCHAR) - error_string_len, NULL);

    // we don't check the return value of FormatMessage because:
    // Some error code description saves in additional modules, normally in a dll, e.g: winhttp.dll
    // In this case, FormatMessage failed but the error_string_buffer contains the error code
    // so we also should return the string
    return error_string_buffer;
}

BOOL ss_utils_get_registry_value(PCTSTR key, PCTSTR entry, PTSTR value, DWORD *valsize)
{
    HKEY reg_key;
    LONG ret;

    ss_return_val_if_fail(!ss_utils_is_string_empty(key), FALSE);
    ss_return_val_if_fail(!ss_utils_is_string_empty(entry), FALSE);
    ss_return_val_if_fail(value != NULL, FALSE);

    // open the registry key
    ret = RegOpenKeyEx(HKEY_CURRENT_USER, key, 0, KEY_QUERY_VALUE, &reg_key);
    if (ret != ERROR_SUCCESS) return FALSE;

    // query the key value
    ret = RegQueryValueEx(reg_key, entry, NULL, NULL, (LPBYTE)value, valsize);
    if (ret != ERROR_SUCCESS) {
        RegCloseKey(reg_key);
        return FALSE;
    }

    RegCloseKey(reg_key);
    return TRUE;
}

BOOL ss_utils_convert_tchar_to_char(PCTSTR from, char **to)
{
    ss_return_val_if_fail(from != NULL, FALSE);
    ss_return_val_if_fail(to != NULL, FALSE);

#ifndef _UNICODE
    *to = strdup(from);
    return TRUE;
#else
    // Call WideCharToMultiByte, get result string length first
    int result_len = WideCharToMultiByte(CP_ACP, 0, from, -1, *to, 0, NULL, NULL);
    if (result_len == 0) return FALSE;
    *to = (char *)malloc(result_len);
    // Call again, to get the final result string
    result_len = WideCharToMultiByte(CP_ACP, 0, from, -1, *to, result_len, NULL, NULL);
    if (result_len == 0) {
        free(*to);
        *to = NULL;
        return FALSE;
    }

    return TRUE;
#endif

}

static BOOL recursive_find_files(map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp> &container, PCTSTR folder)
{
    size_t folder_len;
    TCHAR folder_find_path[MAX_PATH];
    TCHAR recursive_path[MAX_PATH];
    TCHAR file_path[MAX_PATH];
    HANDLE hFind;
    WIN32_FIND_DATA wfd;
    BOOL result = TRUE;
    BOOL dir_empty;
    SSFileInfo *finfo = NULL;
    PTSTR key = NULL;

    StringCchLength(folder, MAX_PATH, &folder_len);
    if (folder[folder_len - 1] == (TCHAR)'\\') {
        StringCchPrintf(folder_find_path, _countof(folder_find_path), TEXT("%s%s"), folder, TEXT("*.*"));
    } else {
        StringCchPrintf(folder_find_path, _countof(folder_find_path), TEXT("%s%s"), folder, TEXT("\\*.*"));
    }

    hFind = FindFirstFile(folder_find_path, &wfd);
    if (hFind == INVALID_HANDLE_VALUE) {
        SSLOG(LOG_LEVEL_ERROR, TEXT("FindFirstFile of folder: %s failed. Reason: %s"), folder_find_path, 
                                    ss_utils_format_error_string(GetLastError()));
        return FALSE;
    } else {
        // Ignore system file and dot files
        if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) || 
            (_tcscmp(wfd.cFileName, TEXT(".")) == 0) || 
            (_tcscmp(wfd.cFileName, TEXT("..")) == 0)) {
            goto FINDNEXT;
        }

        // If the first file is a directory, recursive it first
        if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            if (folder[folder_len - 1] == (TCHAR)'\\') {
                StringCchPrintf(recursive_path, _countof(recursive_path), TEXT("%s%s"), folder, wfd.cFileName);
            } else {
                StringCchPrintf(recursive_path, _countof(recursive_path), TEXT("%s\\%s"), folder, wfd.cFileName);
            }
            // If this is an empty folder, save it into container directly, no need to recursive anymore
            if (ss_utils_is_dir_empty(recursive_path, &dir_empty) && dir_empty) {
                finfo = (SSFileInfo *)malloc(sizeof(SSFileInfo));
                finfo->last_write_time = wfd.ftLastWriteTime;
                finfo->file_size_high = wfd.nFileSizeHigh;
                finfo->file_size_low = wfd.nFileSizeLow;
                if (folder[folder_len - 1] == (TCHAR)'\\') {
                    StringCchPrintf(file_path, _countof(file_path), TEXT("%s%s"), folder, wfd.cFileName);
                } else {
                    StringCchPrintf(file_path, _countof(file_path), TEXT("%s\\%s"), folder, wfd.cFileName);
                }
                StringCchCopy(finfo->file_name, _countof(finfo->file_name), file_path);
                finfo->is_folder = TRUE;
                
                key = (PTSTR)malloc((_tcslen(file_path) + 1) * sizeof(TCHAR));
                memset(key, 0, (_tcslen(file_path) + 1) * sizeof(TCHAR));
                StringCchCopy(key, _tcslen(file_path) + 1, file_path);
                container[key] = finfo;
            } else {
                result = recursive_find_files(container, recursive_path);
                if (result == FALSE) {
                    SSLOG(LOG_LEVEL_ERROR, TEXT("Recursive folder: %s failed."), recursive_path);
                    return FALSE;
                }
            }
        } else {
            // This is a normal file, add it into container
            finfo = (SSFileInfo *)malloc(sizeof(SSFileInfo));
            finfo->last_write_time = wfd.ftLastWriteTime;
            finfo->file_size_high = wfd.nFileSizeHigh;
            finfo->file_size_low = wfd.nFileSizeLow;
            if (folder[folder_len - 1] == (TCHAR)'\\') {
                StringCchPrintf(file_path, _countof(file_path), TEXT("%s%s"), folder, wfd.cFileName);
            } else {
                StringCchPrintf(file_path, _countof(file_path), TEXT("%s\\%s"), folder, wfd.cFileName);
            }
            StringCchCopy(finfo->file_name, _countof(finfo->file_name), file_path);
            finfo->is_folder = FALSE;
            
            key = (PTSTR)malloc((_tcslen(file_path) + 1) * sizeof(TCHAR));
            memset(key, 0, (_tcslen(file_path) + 1) * sizeof(TCHAR));
            StringCchCopy(key, _tcslen(file_path) + 1, file_path);
            container[key] = finfo;
        }

FINDNEXT:
        while (FindNextFile(hFind, &wfd)) {
            if ((wfd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) || 
                (_tcscmp(wfd.cFileName, TEXT(".")) == 0) || 
                (_tcscmp(wfd.cFileName, TEXT("..")) == 0)) {
                continue;
            }
            
            if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                if (folder[folder_len - 1] == (TCHAR)'\\') {
                    StringCchPrintf(recursive_path, _countof(recursive_path), TEXT("%s%s"), folder, wfd.cFileName);
                } else {
                    StringCchPrintf(recursive_path, _countof(recursive_path), TEXT("%s\\%s"), folder, wfd.cFileName);
                }
                // If this is an empty folder, save it into container directly, no need to recursive anymore
                if (ss_utils_is_dir_empty(recursive_path, &dir_empty) && dir_empty) {
                    finfo = (SSFileInfo *)malloc(sizeof(SSFileInfo));
                    finfo->last_write_time = wfd.ftLastWriteTime;
                    finfo->file_size_high = wfd.nFileSizeHigh;
                    finfo->file_size_low = wfd.nFileSizeLow;
                    if (folder[folder_len - 1] == (TCHAR)'\\') {
                        StringCchPrintf(file_path, _countof(file_path), TEXT("%s%s"), folder, wfd.cFileName);
                    } else {
                        StringCchPrintf(file_path, _countof(file_path), TEXT("%s\\%s"), folder, wfd.cFileName);
                    }
                    StringCchCopy(finfo->file_name, _countof(finfo->file_name), file_path);
                    finfo->is_folder = TRUE;
                    
                    key = (PTSTR)malloc((_tcslen(file_path) + 1) * sizeof(TCHAR));
                    memset(key, 0, (_tcslen(file_path) + 1) * sizeof(TCHAR));
                    StringCchCopy(key, _tcslen(file_path) + 1, file_path);
                    container[key] = finfo;
                } else {
                    result = recursive_find_files(container, recursive_path);
                    if (result == FALSE) {
                        SSLOG(LOG_LEVEL_ERROR, TEXT("Recursive folder: %s failed."), recursive_path);
                        return FALSE;
                    }
                }
            } else {
                // This is a normal file, add it into container
                finfo = (SSFileInfo *)malloc(sizeof(SSFileInfo));
                finfo->last_write_time = wfd.ftLastWriteTime;
                finfo->file_size_high = wfd.nFileSizeHigh;
                finfo->file_size_low = wfd.nFileSizeLow;
                if (folder[folder_len - 1] == (TCHAR)'\\') {
                    StringCchPrintf(file_path, _countof(file_path), TEXT("%s%s"), folder, wfd.cFileName);
                } else {
                    StringCchPrintf(file_path, _countof(file_path), TEXT("%s\\%s"), folder, wfd.cFileName);
                }
                StringCchCopy(finfo->file_name, _countof(finfo->file_name), file_path);
                finfo->is_folder = FALSE;
                
                key = (PTSTR)malloc((_tcslen(file_path) + 1) * sizeof(TCHAR));
                memset(key, 0, (_tcslen(file_path) + 1) * sizeof(TCHAR));
                StringCchCopy(key, _tcslen(file_path) + 1, file_path);
                container[key] = finfo;
            }
        }
    }
    if (hFind && hFind != INVALID_HANDLE_VALUE) FindClose(hFind);
    return TRUE;
}

BOOL ss_utils_read_folder(map<PTSTR, SSFileInfo *, SSMapPtstrKeyCmp> &container, PCTSTR folder)
{
    ss_return_val_if_fail(folder != NULL, FALSE);

    return recursive_find_files(container, folder);
}

BOOL ss_utils_get_upper_dir(PCTSTR dir, PTSTR result, BOOL *no_upper_dir)
{
    PTSTR final_slash = NULL;
    PTSTR tmp = NULL;
    TCHAR dir_copy[MAX_PATH];
    TCHAR *fixed_dir = NULL;
    size_t str_len;

    ss_return_val_if_fail(dir != NULL, FALSE);
    ss_return_val_if_fail(result != NULL, FALSE);
    ss_return_val_if_fail(no_upper_dir != NULL, FALSE);

    // Copy the input dir to our internal dir, remove spaces and trailing slash
    StringCchCopy(dir_copy, _countof(dir_copy), dir);
    fixed_dir = ss_utils_trim_both(dir_copy);
    str_len = _tcslen(fixed_dir);
    if ((*(fixed_dir + str_len - 1)) == (TCHAR)'\\') {
        *(fixed_dir + str_len - 1) = (TCHAR)'\0';
    }

    // Loop the string and find out the last slash
    tmp = fixed_dir;
    while (*tmp) {
        if ((*tmp) == (TCHAR)'\\') {
            final_slash = tmp;
        }
        tmp++;
    }

    // The result
    if (final_slash == NULL) {
        *no_upper_dir = TRUE;
    } else {
        *no_upper_dir = FALSE;
        memcpy(result, fixed_dir, (final_slash - fixed_dir) * sizeof(TCHAR));
        result[final_slash - fixed_dir] = (TCHAR)'\0';
    }

    return TRUE;
}

BOOL ss_utils_create_directory(PCTSTR dirpath)
{
    TCHAR upper_level_dir[MAX_PATH];
    BOOL no_upper_dir;

    ss_return_val_if_fail(dirpath != NULL, FALSE);

    if (ss_utils_dir_exists(dirpath)) return TRUE;
    
    // Create uplevel directory
    ss_utils_get_upper_dir(dirpath, upper_level_dir, &no_upper_dir);
    if (!no_upper_dir) {
        ss_utils_create_directory(upper_level_dir);
    }
    // Create current level directory
    if (CreateDirectory(dirpath, NULL) == 0) {
        SSLOG(LOG_LEVEL_ERROR, TEXT("Create directory: %s failed. Reason: %s"), dirpath, ss_utils_format_error_string(GetLastError()));
        return FALSE;
    }
    return TRUE;
}

BOOL ss_utils_get_file_dir(PCTSTR filepath, PTSTR result)
{
    PTSTR final_slash = NULL;
    PTSTR tmp = NULL;
    TCHAR filepath_copy[MAX_PATH];
    TCHAR *fixed_filepath = NULL;
    size_t str_len;

    ss_return_val_if_fail(filepath != NULL, FALSE);
    ss_return_val_if_fail(result != NULL, FALSE);

    // Copy the input filepath to our internal filepath, remove spaces
    StringCchCopy(filepath_copy, _countof(filepath_copy), filepath);
    fixed_filepath = ss_utils_trim_both(filepath_copy);
    str_len = _tcslen(fixed_filepath);
    if ((*(fixed_filepath + str_len - 1)) == (TCHAR)'\\') {
        SSLOG(LOG_LEVEL_ERROR, TEXT("Found illegal file path: %s."), fixed_filepath);
        return FALSE;
    }

    // Loop the string and find out the last slash
    tmp = fixed_filepath;
    while (*tmp) {
        if ((*tmp) == (TCHAR)'\\') {
            final_slash = tmp;
        }
        tmp++;
    }

    // The result
    if (final_slash == NULL) {
        // Current directory is the dir of the file
        result[0] = (TCHAR)'.';
        result[1] = (TCHAR)'\0';
    } else {
        memcpy(result, fixed_filepath, (final_slash - fixed_filepath) * sizeof(TCHAR));
        result[final_slash - fixed_filepath] = (TCHAR)'\0';
    }

    return TRUE;
}